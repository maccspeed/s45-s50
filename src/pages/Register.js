import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom'
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register(){
	const {user} = useContext(UserContext);

	// State hooks to store the values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
    const [mobileNumber, setMobileNumber] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether the submit buttion is enabled or not
	const [isActive, setIsActive] = useState(false);

	// console.log(email);
	// console.log(password1);
	// console.log(password2);

	// Function to simulate user registration
	function registerUser(e){
		e.preventDefault();

        setFirstName('');
        setLastName('');
		setEmail('');
        setMobileNumber('');
		setPassword1('');
		setPassword2('');

        fetch('http://localhost:4000/users/register', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            firstName: firstName,
            lastName: lastName,
            email: email,
            mobileNumber: mobileNumber,
            password: password1
          })
        })
        .then(res => res.json())
        .then(data => {
            
        }
	}

	useEffect(() => {
		// Validate to enable submit button when all fields are populated and both passwords match
		if((firstName !== '' && lastName !== '' && email !== '' && mobileNumber.length >= 11 && password1 !== '' && password2 !== '') && (password1 === password2))
		{
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [firstName, lastName, email, mobileNumber, password1, password2])

	return (
		(user.id !== null) ?
			<Redirect to="/courses" />
			:
			<Form onSubmit={(e) => registerUser(e)}>
			{/*Bind the input states via 2-way binding*/}
			  
              <Form.Group controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter first name"
                    value={firstName}
                    onChange = { e => setFirstName(e.target.value)}
                    required 
                />
              </Form.Group>
              <Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter last name" 
                    value={lastName}
                    onChange={ e => setLastName(e.target.value)}
                    required />
              </Form.Group>

			  <Form.Group controlId="userEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    	type="email" 
			    	placeholder="Enter email"
			    	value={email}
			    	onChange = { e => setEmail(e.target.value)}
			    	required 
			    />
			    <Form.Text className="text-muted">
			      We'll never share your email with anyone else.
			    </Form.Text>
			  </Form.Group>

              <Form.Group controlId="mobileNumber">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="number" 
                    placeholder="Enter Mobile Number" 
                    value={mobileNumber}
                    onChange={ e => setMobileNumber(e.target.value)}
                    required />
              </Form.Group>

			  <Form.Group controlId="password1">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Password" 
			    	value={password1}
			    	onChange={ e => setPassword1(e.target.value)}
			    	required />
			  </Form.Group>
			  <Form.Group controlId="password2">
			  	<Form.Label>Verify Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Verify Password" 
			    	value={password2}
			    	onChange={ e => setPassword2(e.target.value)}
			    	required />
			  </Form.Group>
			{/* Conditionally render the submit button based on isActive state */}
				{ isActive ? 
					<Button variant="primary" type="submit" id="submitBtn">
					    Submit
					</Button>
					:
					<Button variant="danger" type="submit" id="submitBtn" disabled>
					  Submit
					</Button>
				 }
			  
			</Form>
	)
}